import React from 'react'

export default (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xlink='http://www.w3.org/1999/xlink'
    viewBox='0 0 962.32 990'>
    <defs>
      <linearGradient
        id='a'
        x1='170.21'
        y1='768.21'
        x2='439.75'
        y2='27.75'
        gradientTransform='translate(-141.67 209.78) rotate(-21.01)'
        gradientUnits='userSpaceOnUse'>
        <stop offset='0' stopColor='#5f0097' />
        <stop offset='1' stopColor='#30004c' />
      </linearGradient>
      <linearGradient
        id='b'
        x1='432.12'
        y1='815.34'
        x2='717.06'
        y2='32.59'
        href='#a'
      />
    </defs>
    <path
      d='M392.4,421.23C327.74,415.61,224.14,460,224.14,460l83.4-149,101.55-13.64L555.21,105.91l-469.89,106L17.9,933.87l74.24-33.19Z'
      transform='translate(-17.9 -10)'
      fill='url(#a)'
    />
    <path
      d='M719.11,68.92,709.28,222.5,562.69,414.57l13.63,101.55L454.54,635.89S492,526.65,451.28,459.58L115,890.47l845-377.75L980.23,10Z'
      transform='translate(-17.9 -10)'
      fill='url(#b)'
    />
    <path
      d='M555.67,103.84l-147.25,193-102,13.72L222.63,460.33s104.08-44.66,169.05-39L29.41,1000,450.84,459.84c40.89,67.37,3.3,177.13,3.3,177.13L576.48,516.61l-13.72-102L710,221.58l11.15-174.7-165.55,56.9Z'
      transform='translate(-17.9 -10)'
      fill='#fff'
    />
  </svg>
)
