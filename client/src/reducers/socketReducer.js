import { CREATE_SOCKET } from '../actions/types'

const INITIAL_STATE = {
  socket: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_SOCKET:
      return { ...state, socket: action.payload }
    default:
      return state
  }
}
