import { SIGN_IN, CHECK_USERNAME } from '../actions/types'

const INITIAL_STATE = {
  isSignedIn: null,
  isUserNameTaken: false,
  userName: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SIGN_IN:
      return { ...state, isSignedIn: true, userName: action.payload }
    case CHECK_USERNAME:
      return { ...state, isUserNameTaken: action.payload }
    default:
      return state
  }
}
