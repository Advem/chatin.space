import _ from 'lodash'
import {
  FETCH_USERS,
  ADD_USER,
  REMOVE_USER,
  REMOVE_MESSAGE,
  UPDATE_MESSAGES,
  ENABLE_USERS_REORDER
} from '../actions/types'

const INITIAL_STATE = {
  users: null,
  usersReorder: false,
  usersOrder: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_USERS: {
      state = { ...state, users: _.mapKeys(action.payload, 'name') }
      state = {
        ...state,
        users: _.forIn(state.users, (value, key) => {
          _.assign(state.users[key], { lastMessage: 1 })
        })
      }
      return state
    }
    case ADD_USER: {
      return {
        ...state,
        users: {
          ...state.users,
          [action.payload.name]: action.payload
        }
      }
    }
    case REMOVE_USER: {
      return { ...state, users: _.omit(state.users, [action.payload]) }
    }
    case REMOVE_MESSAGE: {
      return {
        ...state,
        users: {
          ...state.users,
          [action.payload.userName]: {
            ...state.users[action.payload.userName],
            messages: _.omit(state.users[action.payload.userName]['messages'], [
              action.payload.timestamp
            ])
          }
        }
      }
    }
    case UPDATE_MESSAGES: {
      if (_.has(state.users, action.payload.userName)) {
        _.assign(state.users[action.payload.userName], {
          lastMessage: +new Date()
        })
        state = {
          ...state,
          usersOrder: _.pull(state.usersOrder, `${action.payload.userName}`)
        }
        state = {
          ...state,
          usersOrder: _.concat(state.usersOrder, `${action.payload.userName}`)
        }
        state = {
          ...state,
          users: {
            ...state.users,
            [action.payload.userName]: {
              ...state.users[action.payload.userName],
              messages: _.assign(
                state.users[action.payload.userName]['messages'],
                { [+new Date()]: action.payload.message }
              )
            }
          }
        }
      }
      // Object reorder based on newest message but it cause
      // major change in DOM so all other Messages timers but newestSender RESET
      // so I set style.order: 1 of UserContainer instead
      // if (state.usersReorder) {
      //   let array = _.orderBy(state.users, ['lastMessage'])
      //   let object = _.keyBy(array, 'name')
      //   return { ...state, users: object }
      // } else return state
      return state
    }
    case ENABLE_USERS_REORDER: {
      if (state.usersReorder) return { ...state, usersReorder: false }
      else return { ...state, usersReorder: true }
    }
    default:
      return state
  }
}
