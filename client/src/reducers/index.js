import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import authReducer from './authReducer'
import menuReducer from './menuReducer'
import roomReducer from './roomReducer'
import usersReducer from './usersReducer'
import settingsReducer from './settingsReducer'
import socketReducer from './socketReducer'
import serverMessagesReducer from './serverMessagesReducer'

export default combineReducers({
  auth: authReducer,
  form: formReducer,
  menu: menuReducer,
  room: roomReducer,
  users: usersReducer,
  settings: settingsReducer,
  socket: socketReducer,
  serverMessages: serverMessagesReducer
})
