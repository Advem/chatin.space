import { SET_APP_COLOR, SET_USER_COLOR } from '../actions/types'

const INITIAL_STATE = {
  appColor: 274,
  userColor: 267
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_APP_COLOR:
      return { ...state, appColor: action.payload }
    case SET_USER_COLOR:
      return { ...state, userColor: action.payload }
    default:
      return state
  }
}
