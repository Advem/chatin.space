import _ from 'lodash'
import {
  INSPECT_ROOM,
  SELECT_ROOM,
  DEINSPECT_ROOM,
  CREATE_ROOM,
  FETCH_ROOMS,
  UPDATE_ROOMS,
  UPDATE_ROOM_USERS
} from '../actions/types'

const INITIAL_STATE = {
  selectedRoom: { id: null, name: null },
  inspectedRoom: null,
  rooms: {}
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SELECT_ROOM:
      return { ...state, selectedRoom: action.payload }
    case INSPECT_ROOM:
      return { ...state, inspectedRoom: action.payload }
    case DEINSPECT_ROOM:
      return { ...state, inspectedRoom: null }
    case CREATE_ROOM:
      return {
        ...state,
        rooms: { ...state.rooms, [action.payload.id]: action.payload }
      }
    // case DELETE_ROOM:
    //   return _.omit(state.rooms, action.payload)
    case FETCH_ROOMS:
      return {
        ...state,
        rooms: _.mapKeys(action.payload, 'id')
      }
    case UPDATE_ROOMS:
      return {
        ...state,
        rooms: { ...state.rooms, [action.payload.id]: action.payload }
      }
    case UPDATE_ROOM_USERS:
      return {
        ...state,
        rooms: {
          ...state.rooms,
          [action.payload.id]: {
            ...state.rooms[action.payload.id],
            users: action.payload.users
          }
        }
      }
    default:
      return state
  }
}
