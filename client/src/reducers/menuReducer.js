import {
  OPEN_MENU,
  CLOSE_MENU,
  OPEN_SETTINGS,
  CLOSE_SETTINGS,
  OPEN_ROOM_FORM,
  CLOSE_ROOM_FORM
} from '../actions/types'

const INITIAL_STATE = {
  isMenuOn: false,
  isSettingsOn: false,
  isRoomFormOn: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case OPEN_MENU:
      return { ...state, isMenuOn: true }
    case CLOSE_MENU:
      return { ...state, isMenuOn: false }
    case OPEN_SETTINGS:
      return { ...state, isSettingsOn: true }
    case CLOSE_SETTINGS:
      return { ...state, isSettingsOn: false }
    case OPEN_ROOM_FORM:
      return { ...state, isRoomFormOn: true }
    case CLOSE_ROOM_FORM:
      return { ...state, isRoomFormOn: false }
    default:
      return state
  }
}
