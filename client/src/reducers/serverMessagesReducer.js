import { ADD_SERVER_MESSAGE, DELETE_SERVER_MESSAGE } from '../actions/types'

const INITIAL_STATE = {
  messages: {}
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_SERVER_MESSAGE: {
      return { ...state, messages: action.payload }
    }
    case DELETE_SERVER_MESSAGE: {
      return { ...state, messages: null }
    }
    default:
      return state
  }
}
