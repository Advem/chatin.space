import React from 'react'
import { connect } from 'react-redux'
import { selectRoom, deInspectRoom } from '../../actions'
import EnterRoomForm from './EnterRoomForm'

import style from '../../assets/styles/RoomDetails.module.scss'
import button from '../../assets/styles/Button.module.scss'
import info from '../../assets/styles/Info.module.scss'

import timerIcon from '../../assets/icons/history-solid'
import lockIcon from '../../assets/icons/lock-solid'
import unlockIcon from '../../assets/icons/unlock-solid'
import usersIcon from '../../assets/icons/users-solid'
import crownIcon from '../../assets/icons/crown-solid'

const RoomDetails = ({
  isSelected,
  isInspected,
  isSignedIn,
  room,
  selectRoom,
  deInspectRoom
}) => {
  const onRoomSelect = () => {
    selectRoom(room.id, room.name)
    deInspectRoom()
  }

  const renderEnterButton = () => {
    if (isSignedIn) {
      if (room.users >= room.limit && !isSelected) {
        return (
          <div className={info.Access}>
            <b>No</b> more <b>Space</b> in this Galaxy
          </div>
        )
      } else {
        if (isSelected) {
          return (
            <div className={info.Access}>
              <b>{room.name}</b> Entered
            </div>
          )
        } else {
          if (room.privacy) {
            return <EnterRoomForm room={room} />
          } else {
            return (
              <div className={button.White} onClick={() => onRoomSelect()}>
                ENTER GALAXY
              </div>
            )
          }
        }
      }
    } else {
      return (
        <div className={info.Access}>
          <b>Sign in</b> to Enter this Galaxy
        </div>
      )
    }
  }

  return (
    <div
      className={
        isInspected ? `${style.Details}` : `${style.Details} ${style.Hidden}`
      }>
      <div className={style.Options}>
        <div
          className={style.Option}
          title={`Messages disappear after ${room.timeout} seconds`}>
          {timerIcon}
          {room.timeout}s
        </div>
        {room.privacy ? (
          <div className={style.Option} title={`Room is Private`}>
            {lockIcon}
            Private
          </div>
        ) : (
          <div className={style.Option} title={`Room is Public`}>
            {unlockIcon}
            Public
          </div>
        )}
        <div
          className={style.Option}
          title={`Room has been created by ${room.userName}`}>
          {crownIcon}
          <span>{room.userName}</span>
        </div>
        <div
          className={style.Option}
          title={`Room capacity is ${room.limit} users`}>
          {usersIcon}
          {room.limit}
        </div>
        {renderEnterButton()}
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps, { selectRoom, deInspectRoom })(
  RoomDetails
)
