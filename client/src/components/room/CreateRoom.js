import React from 'react'
import { connect } from 'react-redux'
import { openRoomForm, closeRoomForm } from '../../actions'
import CreateRoomForm from './CreateRoomForm'

import style from '../../assets/styles/CreateRoom.module.scss'
import info from '../../assets/styles/Info.module.scss'

const CreateRoom = ({
  isSignedIn,
  isRoomFormOn,
  openRoomForm,
  closeRoomForm
}) => {
  const expandForm = () => {
    if (isRoomFormOn) closeRoomForm()
    else openRoomForm()
  }

  return (
    <div
      className={
        isRoomFormOn
          ? isSignedIn
            ? `${style.Container} ${style.Active}`
            : `${style.Container} ${style.NotSignedIn}`
          : style.Container
      }>
      <div className={style.Header} onClick={() => expandForm()}>
        <div className={style.Plus}>
          <div className={style.PlusOne}></div>
          <div className={style.PlusTwo}></div>
        </div>
        <div className={style.Title}>New Galaxy</div>
      </div>
      {isRoomFormOn ? (
        isSignedIn ? (
          <CreateRoomForm />
        ) : (
          <div className={info.Access}>Sign in to create New Galaxy</div>
        )
      ) : null}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn,
    isRoomFormOn: state.menu.isRoomFormOn
  }
}

export default connect(
  mapStateToProps,
  { openRoomForm, closeRoomForm }
)(CreateRoom)
