import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { selectRoom, deInspectRoom } from '../../actions'
import { sha256 } from 'js-sha256'

import form from '../../assets/styles/Form.module.scss'

class EnterRoomForm extends React.Component {
  renderInput = ({ input, meta }) => {
    return (
      <input
        {...input}
        className={
          meta.error && meta.touched
            ? `${form.PasswordInput} ${form.ErrorInput}`
            : form.PasswordInput
        }
        placeholder='Password required...'
        type='password'
        autoComplete='off'
        maxLength='20'
      />
    )
  }

  onSubmit = formValues => {
    const userInput = sha256(formValues.password)
    if (userInput === this.props.room.password) {
      this.props.selectRoom(this.props.room.id, this.props.room.name)
      this.props.deInspectRoom()
    }
    // else {
    //   // console.log('Incorrect Password')
    // }
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Field name='password' component={this.renderInput} />
      </form>
    )
  }
}

const validate = (formValues, props) => {
  const errors = {}

  if (!formValues.password) {
    errors.password = 'Password is required!'
  }

  const userInput = sha256(`${formValues.password}`)

  if (userInput !== props.room.password) {
    errors.password = 'Incorrect password!'
  }

  return errors
}

export default connect(
  null,
  { selectRoom, deInspectRoom }
)(
  reduxForm({
    form: 'enterRoom',
    validate: validate
  })(EnterRoomForm)
)
