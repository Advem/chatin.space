import React from 'react'
import { connect } from 'react-redux'
import Room from './Room'
import CreateRoom from './CreateRoom'

import { css } from 'glamor'
import style from '../../assets/styles/RoomList.module.scss'

const RoomList = ({ isMenuOn, rooms, appColor }) => {
  const color = css({
    background: `linear-gradient(338deg, hsla(${appColor}, 60%, 17%, 0.1), hsla(${appColor}, 100%, 50%, 0.1))`
  })

  const renderRooms = () => {
    if (isMenuOn && rooms) {
      return (
        <div className={style.UniverseRooms}>
          <div className={style.Header}>Universe</div>
          {rooms
            .filter(room => room.id <= 8)
            .map(room => {
              return <Room key={room.id} id={room.id} />
            })}
          <div className={style.Header}>Discovered Galaxies</div>
          <CreateRoom />
          {rooms
            .filter(room => room.id > 8)
            .map(room => {
              return <Room key={room.id} id={room.id} />
            })}
        </div>
      )
    } else if (isMenuOn && !rooms) {
      return <div>Loading...</div>
    }
  }

  return (
    <div
      className={
        isMenuOn
          ? `${style.RoomList} ${style.Active} ${color}`
          : `${style.RoomList} ${color}`
      }>
      {renderRooms()}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isMenuOn: state.menu.isMenuOn,
    rooms: Object.values(state.room.rooms),
    appColor: state.settings.appColor
  }
}

export default connect(
  mapStateToProps,
  {}
)(RoomList)
