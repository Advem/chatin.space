import React from 'react'
import { connect } from 'react-redux'
import { inspectRoom, deInspectRoom } from '../../actions'
import RoomDetails from './RoomDetails'

import style from '../../assets/styles/Room.module.scss'
import astronautIcon from '../../assets/icons/user-astronaut-solid'

const Room = ({
  selectedRoom,
  inspectedRoom,
  inspectRoom,
  deInspectRoom,
  id,
  room
}) => {
  const toggleInspectRoom = () => {
    if (inspectedRoom === id) deInspectRoom()
    else if (!inspectedRoom || inspectedRoom !== id) inspectRoom(id)
  }

  const renderRoomDetails = () => {
    if (room) {
      return (
        <RoomDetails
          room={room}
          isInspected={inspectedRoom === id ? true : false}
          isSelected={selectedRoom.id === id ? true : false}
        />
      )
    }
  }

  return (
    <div className={style.Container}>
      <div
        className={
          selectedRoom.id === id
            ? inspectedRoom === id
              ? `${style.RoomHovered} ${style.RoomSelected}`
              : `${style.Room} ${style.RoomSelected}`
            : inspectedRoom === id
            ? style.RoomHovered
            : style.Room
        }
        onClick={() => toggleInspectRoom()}>
        <div className={style.Title}>{room ? room.name : 'Galaxy'}</div>
        <div className={style.Users}>
          <span>{room.users ? room.users : 0}</span>
          {astronautIcon}
        </div>
      </div>
      {renderRoomDetails()}
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    selectedRoom: state.room.selectedRoom,
    inspectedRoom: state.room.inspectedRoom,
    room: state.room.rooms[ownProps.id]
  }
}

export default connect(mapStateToProps, { inspectRoom, deInspectRoom })(Room)
