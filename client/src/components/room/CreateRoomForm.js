import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { createRoom, closeRoomForm } from '../../actions'
import { sha256 } from 'js-sha256'
import _ from 'lodash'

import form from '../../assets/styles/Form.module.scss'
import button from '../../assets/styles/Button.module.scss'

import lockIcon from '../../assets/icons/lock-solid'
import unlockIcon from '../../assets/icons/unlock-solid'

class CreateRoomForm extends React.Component {
  renderError = ({ error, touched }) => {
    if (touched && error) return <div className={form.Error}>{error}</div>
  }

  renderTextInput = ({ input, placeholder, type, label, meta }) => {
    return (
      <div>
        <div className={form.Label}>
          <div className={form.LabelTitle}>{label}:</div>
          <div className={form.LabelValue}>
            {this.renderError(meta)}
            {type === 'password' && input.value !== ''
              ? sha256(input.value)
              : input.value}
          </div>
        </div>
        <div className={form.InputContainer}>
          <input
            {...input}
            className={
              meta.error && meta.touched
                ? `${form.TextInput} ${form.ErrorInput}`
                : form.TextInput
            }
            placeholder={placeholder}
            autoComplete='off'
            maxLength='20'
            type={type}
          />
        </div>
      </div>
    )
  }

  renderRangeInput = ({ input, label, meta }) => {
    return (
      <div>
        <div className={form.Label}>
          <div className={form.LabelTitle}>{label}:</div>
          <div className={form.LabelValue}>
            {this.renderError(meta)}
            {input.value && label === 'Message Timeout'
              ? `${input.value}s`
              : input.value}
          </div>
        </div>
        <div className={form.InputContainer}>
          <input
            {...input}
            className={
              meta.error && meta.touched
                ? `${form.RangeInput} ${form.ErrorInput}`
                : form.RangeInput
            }
            type='range'
            min='1'
            max={label === 'Message Timeout' ? '30' : '20'}
            step={label === 'Message Timeout' ? '1' : '1'}
          />
        </div>
      </div>
    )
  }

  renderCheckBoxInput = ({ input, label, meta }) => {
    return (
      <div>
        <div className={form.Label}>
          <div className={form.LabelTitle}>{label}:</div>
          <div className={form.LabelValue}>
            {this.renderError(meta)}
            {input.value ? `Private` : 'Public'}
          </div>
        </div>
        <div className={form.InputContainer}>
          <div className={form.CheckBoxContainer}>
            <input
              {...input}
              className={
                meta.error && meta.touched
                  ? `${form.CheckBoxContainerInner} ${form.ErrorInput}`
                  : form.CheckBoxContainerInner
              }
              type='checkbox'
              value={input.value ? input.value : false}
            />
            <div className={form.Icon}>
              {input.value ? lockIcon : unlockIcon}
            </div>
          </div>
        </div>
      </div>
    )
  }

  onSubmit = formValues => {
    if (formValues.password)
      _.assign(formValues, { password: sha256(formValues.password) }) // encrypt password
    this.props.createRoom(formValues)
    this.props.closeRoomForm()
  }

  render() {
    return (
      <div className={form.FormContainer}>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
          <Field
            name='name'
            placeholder='Name'
            label='Galaxy'
            type='text'
            component={this.renderTextInput}
          />
          {this.props.isRoomPrivate && (
            <Field
              name='password'
              placeholder='Password'
              label='Password'
              type='password'
              component={this.renderTextInput}
            />
          )}
          <Field
            name='privacy'
            label='Privacy'
            component={this.renderCheckBoxInput}
          />
          <Field
            name='timeout'
            label='Message Timeout'
            component={this.renderRangeInput}
          />
          <Field
            name='limit'
            label='Users Limit'
            component={this.renderRangeInput}
          />
          <div className={form.Submit}>
            <button className={button.White}>CREATE</button>
          </div>
        </form>
      </div>
    )
  }
}

const validate = formValues => {
  const errors = {}

  if (!formValues.name) {
    errors.name = 'Your Galaxy has to have a name'
  }

  if (!formValues.password) {
    errors.password = 'Private requires a password'
  }

  if (!formValues.timeout) {
    errors.timeout = 'Set Message vanish time'
  }

  if (!formValues.limit) {
    errors.limit = 'Set maximum Astronauts'
  }

  return errors
}

const mapStateToProps = state => {
  return {
    isRoomPrivate: formValueSelector('createRoom')(state, 'privacy'),
    initialValues: { privacy: false, password: null }
  }
}

export default connect(mapStateToProps, {
  createRoom,
  closeRoomForm
})(
  reduxForm({
    form: 'createRoom',
    validate: validate,
    enableReinitialize: true
  })(CreateRoomForm)
)
