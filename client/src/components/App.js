import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import Menu from './Menu'
import SignIn from './SignIn'
import RoomList from './room/RoomList'
import ChatView from './chat/ChatView'
import Disconnected from './Disconnected'
import Background from './Background'
import { ENDPOINT } from '../ENDPOINT'

import { css } from 'glamor'
import style from '../assets/styles/App.module.scss'
import {
  fetchRooms,
  updateRoomUsers,
  updateRooms,
  createSocket
} from '../actions'

const App = ({
  isSignedIn,
  selectedRoom,
  appColor,
  createSocket,
  socket,
  fetchRooms,
  updateRooms,
  updateRoomUsers
}) => {
  const colorApp = css({
    background: `linear-gradient(181deg, hsl(${appColor}, 80%, 30%), hsl(${appColor}, 70%, 20%) 30%, hsl(${appColor}, 60%, 2%))`
  })

  const colorRed = css({
    background: `linear-gradient(181deg, hsl(2, 80%, 30%), hsl(2, 70%, 20%) 30%, hsl(2, 60%, 5%))`
  })

  useEffect(() => {
    if (!socket) {
      createSocket()
      // console.log('SOCKET CREATED')
    }
  }, [socket, createSocket])

  useEffect(() => {
    if (socket && socket.disconnected) {
      socket.connect(ENDPOINT)
    }
  }, [socket])

  useEffect(() => {
    // console.log('FETCHING ROOMS...')
    fetchRooms()
  }, [socket, fetchRooms])

  useEffect(() => {
    if (socket) {
      socket.on('updateRooms', data => {
        // console.log('ROOMLIST UPDATED', data)
        updateRooms(data)
      })

      socket.on('roomChanged', ({ roomId, users }) => {
        // console.log(`ROOM ${roomId} UPDATED => ${users} users`)
        updateRoomUsers(roomId, users)
      })
    }
    return () => {
      if (socket && socket.connected) {
        // console.log('SOCKET DISCONNECTED')
        socket.emit('disconnect')
        socket.off()
      }
    }
  }, [socket, updateRooms, updateRoomUsers])

  return (
    <div
      className={
        socket && socket.disconnected
          ? `${style.App} ${colorRed}`
          : `${style.App} ${colorApp}`
      }>
      {socket && socket.connected ? (
        <>
          <Background />
          <Menu />
          <RoomList />
          {isSignedIn ? <ChatView selectedRoom={selectedRoom} /> : <SignIn />}
        </>
      ) : (
        <Disconnected />
      )}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn,
    selectedRoom: state.room.selectedRoom,
    appColor: state.settings.appColor,
    socket: state.socket.socket,
    rooms: state.room.rooms
  }
}

export default connect(mapStateToProps, {
  createSocket,
  fetchRooms,
  updateRooms,
  updateRoomUsers
})(App)
