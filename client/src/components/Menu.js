import React from 'react'
import { connect } from 'react-redux'
import { openMenu, closeMenu } from '../actions'

import { css } from 'glamor'
import button from '../assets/styles/Button.module.scss'

const Menu = ({ isMenuOn, openMenu, closeMenu, appColor }) => {
  const color = css({ backgroundColor: `hsl(${appColor}, 40%, 30%)` })

  const toggleMenu = () => {
    if (isMenuOn) closeMenu()
    else openMenu()
  }

  return (
    <div
      className={
        isMenuOn
          ? `${button.MenuButton} ${button.MenuActive}`
          : `${button.MenuButton}`
      }
      onClick={() => toggleMenu()}>
      <div className={`${button.BurgerTop} ${color}`}></div>
      <div className={`${button.BurgerMid} ${color}`}></div>
      <div className={`${button.BurgerBot} ${color}`}></div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isMenuOn: state.menu.isMenuOn,
    appColor: state.settings.appColor
  }
}

export default connect(
  mapStateToProps,
  { openMenu, closeMenu }
)(Menu)
