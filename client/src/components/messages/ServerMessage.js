import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { deleteServerMessage } from '../../actions'

import { css } from 'glamor'
import messages from '../../assets/styles/Messages.module.scss'

const ServerMessage = ({ message, deleteServerMessage, appColor }) => {
  useEffect(() => {
    let timer = setTimeout(() => {
      deleteServerMessage()
    }, 4000)
    return () => {
      clearTimeout(timer)
    }
  })

  const color = css({
    backgroundColor: `hsla(${appColor}, 50%, 50%, 0.2)`,
    color: `hsla(${appColor}, 50%, 75%, 0.5)`
  })

  return (
    <div className={`${messages.ServerMessage} ${color}`}>{`${message}`}</div>
  )
}

const mapStateToProps = state => {
  return { appColor: state.settings.appColor }
}

export default connect(
  mapStateToProps,
  { deleteServerMessage }
)(ServerMessage)
