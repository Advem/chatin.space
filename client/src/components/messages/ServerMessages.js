import React from 'react'
import { connect } from 'react-redux'
import ServerMessage from './ServerMessage'

import messages from '../../assets/styles/Messages.module.scss'

const ServerMessages = ({ message }) => {
  const renderMessages = () => {
    if (message)
      return <ServerMessage message={message} key={`key${message}`} />
  }

  return (
    <div className={messages.ServerMessagesContainer}>{renderMessages()}</div>
  )
}

const mapStateToProps = state => {
  return { message: state.serverMessages.messages }
}

export default connect(
  mapStateToProps,
  {}
)(ServerMessages)
