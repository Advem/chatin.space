import React from 'react'
import { connect } from 'react-redux'
import UserMessage from './UserMessage'

import { css } from 'glamor'
import style from '../../assets/styles/Users.module.scss'

const User = ({
  user,
  userName,
  messages,
  appColor,
  userColor,
  timeout,
  usersOrder,
  usersReorder
}) => {
  const appColorBackground = css({
    backgroundColor: `hsla(${appColor}, 100%, 30%, 0.5)`,
    color: `hsla(${appColor}, 100%, 75%, 1)`
  })

  const userColorBackground = css({
    backgroundColor: `hsla(${userColor}, 100%, 30%, 0.5)`,
    color: `hsla(${userColor}, 100%, 75%, 1)`
  })

  const flexOrder = css({
    order: `${usersReorder ? usersOrder.indexOf(user.name) : '0'}`
  })

  const renderMessages = () => {
    if (messages) {
      return Object.entries(messages).map(([key, value]) => {
        return (
          <UserMessage
            timestamp={key}
            message={value}
            color={user.name === userName ? userColor : appColor}
            timeout={timeout}
            key={`${user.name}_${key}`}
            user={user.name}
          />
        )
      })
    }
  }

  return (
    <div className={`${style.UserContainer} ${flexOrder}`}>
      <div className={style.Messages}>{renderMessages()}</div>
      <div
        className={
          user.name === userName
            ? `${style.User} ${userColorBackground}`
            : `${style.User} ${appColorBackground}`
        }>
        <div className={style.UserName}>{user.name}</div>
      </div>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    usersReorder: state.users.usersReorder,
    usersOrder: state.users.usersOrder,
    userName: state.auth.userName,
    userColor: state.settings.userColor,
    appColor: state.settings.appColor,
    messages: state.users.users[ownProps.user.name].messages,
    timeout: state.room.rooms[state.room.selectedRoom.id]['timeout']
  }
}

export default connect(
  mapStateToProps,
  {}
)(User)
