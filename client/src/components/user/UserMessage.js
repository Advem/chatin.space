import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { removeMessage } from '../../actions'
import ReactEmoji from 'react-emoji'

import { css } from 'glamor'
import style from '../../assets/styles/Users.module.scss'
import element from '../../assets/styles/Element.module.scss'

const UserMessage = ({
  message,
  color,
  user,
  timestamp,
  timeout,
  removeMessage
}) => {
  useEffect(() => {
    let messageTimeout = setTimeout(
      () => removeMessage(user, timestamp),
      timeout * 1000
    )
    return () => {
      clearTimeout(messageTimeout)
    }
  })

  const messageColor = css({ backgroundColor: `hsla(${color}, 100%, 40%, 1)` })

  const fadeOut = css.keyframes({
    '0%': { opacity: 1 },
    '50%': { opacity: 1 },
    '75%': { opacity: 0.75 },
    '100%': { opacity: 0 }
  })

  const messageAnimation = css({
    animation: `${fadeOut} ${timeout}s ease-in-out 1 normal forwards`
  })

  const animationTime = css({
    animationDuration: `${timeout}s`
  })

  return (
    <div className={`${style.Message} ${messageAnimation} ${messageColor}`}>
      {ReactEmoji.emojify(message)}
      <div className={element.Timer}>
        <div className={`${element.Spinner} ${animationTime}`}></div>
        <div
          className={`${element.Left} ${animationTime} ${messageColor}`}></div>
        <div className={`${element.Right} ${animationTime}`}></div>
        <div className={`${element.Center} ${messageColor}`}></div>
      </div>
    </div>
  )
}

export default connect(
  null,
  { removeMessage }
)(UserMessage)
