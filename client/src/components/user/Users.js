import React from 'react'
import { connect } from 'react-redux'
import ScrollToBottom from 'react-scroll-to-bottom'
import User from './User'

import style from '../../assets/styles/Users.module.scss'

const Users = ({ users, usersReorder }) => {
  const renderUsers = () => {
    if (users) {
      return Object.values(users).map((user, id) => {
        return <User user={user} key={id} />
      })
    }
  }

  const renderMode = () => {
    if (usersReorder) {
      return (
        <ScrollToBottom>
          <div className={style.UsersContainer}>{renderUsers()}</div>
        </ScrollToBottom>
      )
    } else return <div className={style.UsersContainer}>{renderUsers()}</div>
  }

  return renderMode()
}

const mapStateToProps = state => {
  return {
    users: state.users.users,
    usersReorder: state.users.usersReorder
  }
}

export default connect(
  mapStateToProps,
  {}
)(Users)
