import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import {
  signIn,
  selectRoom,
  createSocket,
  checkUserName,
  setAppColor
} from '../actions'

import { css } from 'glamor'
import style from '../assets/styles/Modal.module.scss'
import form from '../assets/styles/Form.module.scss'
import button from '../assets/styles/Button.module.scss'
import chatinSpaceIcon from '../assets/icons/chatinSpaceIcon'

class SignIn extends React.Component {
  // componentDidMount() {
  //   this.props.createSocket()
  // }

  renderInput = ({ input, meta, getUserName, isUserNameTaken }) => {
    return (
      <>
        <input
          onChange={getUserName(input.value)}
          className={
            isUserNameTaken
              ? `${form.TextInput} ${form.TextCenter} ${form.ErrorInput}`
              : meta.error && meta.touched
              ? `${form.TextInput} ${form.TextCenter} ${form.ErrorInput}`
              : `${form.TextInput} ${form.TextCenter}`
            // IKR...
          }
          type='text'
          placeholder='Type your Astronaut nickname...'
          autoComplete='off'
          {...input}
        />
        {this.renderError(meta)}
      </>
    )
  }

  renderError = ({ error, touched }) => {
    if (touched && error) {
      return <p className={form.Error}>{error}</p>
    }
  }

  renderUserNameTaken = () => {
    if (this.props.isUserNameTaken) {
      return (
        <p className={form.Error}>
          This name is somewhere there deep in the Space
        </p>
      )
    }
  }

  renderSocketDisconnected = () => {
    if (this.props.socket && this.props.socket.disconnected) {
      this.props.setAppColor(0)
      return (
        <p className={form.Error}>
          The Server has been absorbed by a Black Hole
        </p>
      )
    } else this.props.setAppColor(274)
  }

  onSubmit = formValues => {
    if (!this.props.isUserNameTaken && this.props.socket.connected) {
      this.props.signIn(formValues.userName)
      this.props.selectRoom(1, 'Milky Way')
    }
  }

  getUserName = userName => {
    if (userName) this.props.checkUserName(userName)
  }

  render() {
    const color = css({
      background: `linear-gradient(-10deg, rgba(0, 0, 0, 0) 35%, hsla(${this.props.appColor}, 100%, 50%, 0.1))`
    })

    return (
      <div className={style.OuterContainer}>
        <div className={`${style.InnerContainer} ${color}`}>
          <div className={style.Icon}>{chatinSpaceIcon}</div>
          <h2 className={style.Title}>Welcome, Astronaut!</h2>
          <p className={style.Subtitle}>
            Start your<b> Journey </b>throughout the
            <b> Universe</b>
          </p>
          <form
            className={form.Form}
            onSubmit={this.props.handleSubmit(this.onSubmit)}>
            <Field
              name='userName'
              component={this.renderInput}
              getUserName={this.getUserName}
              isUserNameTaken={this.props.isUserNameTaken}
            />
            {this.renderUserNameTaken()}
            {this.renderSocketDisconnected()}
            <div className={form.Submit}>
              <button className={button.White}>Chat Now</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const validate = formValues => {
  const errors = {}

  if (!formValues.userName) {
    errors.userName = 'You must type your name, Astronaut'
  }

  return errors
}

const mapStateToProps = state => {
  return {
    appColor: state.settings.appColor,
    socket: state.socket.socket,
    isUserNameTaken: state.auth.isUserNameTaken
  }
}

export default connect(mapStateToProps, {
  signIn,
  selectRoom,
  createSocket,
  checkUserName,
  setAppColor
})(
  reduxForm({
    form: 'createUser',
    validate
  })(SignIn)
)
