import React from 'react'
import { connect } from 'react-redux'
import { fetchRooms } from '../actions'

import { css } from 'glamor'
import style from '../assets/styles/Modal.module.scss'
import Element from '../assets/styles/Element.module.scss'
import Button from '../assets/styles/Button.module.scss'

const Disconnected = ({ fetchRooms }) => {
  const colorBackground = css({
    background: `linear-gradient(-10deg, rgba(0, 0, 0, 0) 35%, hsla(2, 100%, 50%, 0.1))`,
    color: `hsla(2, 100%, 55%)`
  })

  const colorLoader = css({
    background: `linear-gradient(-10deg, rgba(0, 0, 0, 0) 35%, hsla(2, 100%, 50%, 0.5))`
  })

  return (
    <div className={style.OuterContainer}>
      <div className={`${style.InnerContainer} ${colorBackground}`}>
        <div className={style.Large}>Black Hole</div>
        <div className={style.Medium}>
          absorbed the <b>Universe</b>
        </div>
        <div className={`${Element.Loader} ${colorLoader}`}></div>
        <div className={style.Small}>Server reconnection in progress...</div>
        <div className={Button.Red} onClick={() => fetchRooms()}>
          Force Reconnect
        </div>
      </div>
    </div>
  )
}

export default connect(
  null,
  { fetchRooms }
)(Disconnected)
