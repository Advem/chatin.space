import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { setAppColor, setUserColor } from '../../actions'

import form from '../../assets/styles/Form.module.scss'

class Settings extends React.Component {
  ColorInput = ({ input, label }) => {
    return (
      <div className={form.ColorSetting}>
        <label className={form.ColorLabel}>{label}</label>
        <input
          {...input}
          type='range'
          min='0'
          max='360'
          className={form.RangeInput}
        />
      </div>
    )
  }

  onSubmit = formValues => {
    this.props.setAppColor(formValues.appColor)
    this.props.setUserColor(formValues.userColor)
  }

  render() {
    return (
      <div className={form.ColorSettings}>
        <form onChange={this.props.handleSubmit(this.onSubmit)}>
          <Field name='appColor' label='Theme' component={this.ColorInput} />
          <Field
            name='userColor'
            label='Astronaut'
            component={this.ColorInput}
          />
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    initialValues: {
      appColor: state.settings.appColor,
      userColor: state.settings.userColor
    }
  }
}

export default connect(
  mapStateToProps,
  { setAppColor, setUserColor }
)(reduxForm({ form: 'colorSettings', enableReinitialize: true })(Settings))
