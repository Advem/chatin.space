import React from 'react'
import { connect } from 'react-redux'
import MessageForm from './MessageForm'

import { css } from 'glamor'
import chat from '../../assets/styles/Chat.module.scss'
import settingIcon from '../../assets/icons/cog-solid'
import reorderEnabled from '../../assets/icons/arrow-alt-circle-down-regular'
import reorderDisabled from '../../assets/icons/stop-circle-regular'
import {
  closeSettings,
  openSettings,
  enableUsersReorder,
  addServerMessage
} from '../../actions'

const ChatMenu = ({
  appColor,
  openSettings,
  closeSettings,
  isSettingsOn,
  enableUsersReorder,
  usersReorder,
  addServerMessage
}) => {
  const color = css({
    '> svg': { opacity: `0.25` },
    backgroundColor: `hsla(${appColor}, 50%, 50%, 0.25)`,
    ':hover': {
      backgroundColor: `hsla(${appColor}, 50%, 50%, 1)`,
      '> svg': { opacity: `1` }
    }
  })
  const colorEnabled = css({
    '> svg': { opacity: `0.66` },
    backgroundColor: `hsla(${appColor}, 75%, 50%, 0.5)`,
    ':hover': { backgroundColor: `hsla(${appColor}, 50%, 50%, 1)` }
  })

  const toggleSettings = () => {
    if (isSettingsOn) closeSettings()
    else openSettings()
  }

  const toggleUsersReorder = () => {
    if (usersReorder) addServerMessage('Disabled auto scroll positioning')
    else addServerMessage('Enabled auto scroll positioning')
    enableUsersReorder()
  }

  return (
    <div className={chat.MessageMenu}>
      <div className={chat.MessageForm}>
        <MessageForm />
      </div>
      <div
        className={
          isSettingsOn
            ? `${chat.Settings} ${colorEnabled} ${chat.SettingsActive}`
            : `${chat.Settings} ${color}`
        }
        onClick={() => toggleSettings()}
        onMouseEnter={() =>
          addServerMessage('Customise Theme and Messages colors')
        }>
        {settingIcon}
      </div>
      <div
        className={
          usersReorder
            ? `${chat.Settings} ${colorEnabled}`
            : `${chat.Settings} ${color}`
        }
        onClick={() => toggleUsersReorder()}
        onMouseEnter={() =>
          addServerMessage(
            `${
              usersReorder ? 'Disable' : 'Enable'
            } Astronauts auto-positioning on new Message`
          )
        }>
        {usersReorder ? reorderEnabled : reorderDisabled}
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    appColor: state.settings.appColor,
    isSettingsOn: state.menu.isSettingsOn,
    usersReorder: state.users.usersReorder
  }
}

export default connect(mapStateToProps, {
  openSettings,
  closeSettings,
  enableUsersReorder,
  addServerMessage
})(ChatMenu)
