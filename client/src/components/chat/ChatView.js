import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import {
  updateRoomUsers,
  addServerMessage,
  fetchUsers,
  addUser,
  removeUser,
  updateMessages
} from '../../actions'

import ChatMenu from './ChatMenu'
import Settings from './Settings'
import Users from '../user/Users'
import ServerMessages from '../messages/ServerMessages'

import chat from '../../assets/styles/Chat.module.scss'

const ChatView = ({
  room,
  isSettingsOn,
  userName,
  socket,
  selectedRoom,
  fetchUsers,
  addUser,
  removeUser,
  updateMessages,
  updateRoomUsers,
  addServerMessage
}) => {
  useEffect(() => {
    socket.emit('join', {
      userName,
      roomName: selectedRoom.name,
      roomId: selectedRoom.id
    })

    return () => {
      socket.emit('leave', {
        userName,
        roomName: selectedRoom.name,
        roomId: selectedRoom.id
      })
    }
  }, [socket, selectedRoom, userName, updateRoomUsers])

  useEffect(() => {
    socket.on('fetchUsers', users => {
      fetchUsers(users)
    })
  }, [socket, fetchUsers])

  useEffect(() => {
    socket.on('addUser', user => {
      // console.log(`ADD USER: ${user}`)
      addUser(user)
    })
  }, [socket, addUser])

  useEffect(() => {
    socket.on('removeUser', user => {
      // console.log(`REMOVE USER: ${user}`)
      removeUser(user)
    })
  }, [socket, removeUser])

  useEffect(() => {
    socket.on('receiveMessage', data => {
      // console.log(`${data.userName} sent ${data.message}`)
      updateMessages(data)
    })
  }, [socket, updateMessages])

  useEffect(() => {
    socket.on('serverMessage', text => {
      addServerMessage(text)
    })
  }, [socket, addServerMessage])

  return (
    <div className={chat.ChatContainer}>
      <div className={chat.RoomName}>{room.name}</div>
      <div className={chat.UserName}>{userName}</div>
      <Users />
      <ServerMessages />
      <ChatMenu />
      {isSettingsOn ? <Settings /> : null}
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    selectedRoom: state.room.selectedRoom,
    room: state.room.rooms[ownProps.selectedRoom.id],
    isSettingsOn: state.menu.isSettingsOn,
    userName: state.auth.userName,
    socket: state.socket.socket
  }
}

export default connect(mapStateToProps, {
  updateRoomUsers,
  addServerMessage,
  fetchUsers,
  addUser,
  removeUser,
  updateMessages
})(ChatView)
