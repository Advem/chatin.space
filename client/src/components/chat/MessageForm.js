import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { sendMessage } from '../../actions'

import { css } from 'glamor'
import form from '../../assets/styles/Form.module.scss'

class MessageForm extends React.Component {
  renderInput = ({ input, appColor }) => {
    const colors = css({
      '::placeholder': {
        color: `hsla(${appColor}, 50%, 75%, 0.66) !important`
      },
      color: `#fff`,
      backgroundColor: `hsla(${appColor}, 50%, 50%, 0.33)`
    })
    return (
      <input
        {...input}
        className={`${form.MessageInput} ${colors}`}
        placeholder='Type a message...'
        autoComplete='off'
        type='text'
        maxLength='120'
      />
    )
  }

  onSubmit = formValues => {
    // console.log(formValues)
    if (formValues.message && formValues.message.replace(/\s/g, '').length > 0)
      this.props.sendMessage(formValues.message)
  }

  render() {
    return (
      <form
        className={form.FormContainer}
        onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Field
          name='message'
          component={this.renderInput}
          appColor={this.props.appColor}
        />
      </form>
    )
  }
}

const mapStateToProps = state => {
  return {
    appColor: state.settings.appColor
  }
}

export default connect(mapStateToProps, { sendMessage })(
  reduxForm({
    form: 'createMessage'
  })(MessageForm)
)
