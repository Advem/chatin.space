import React from 'react'

import background from '../assets/styles/Background.module.scss'
import stars from '../assets/script-tutorials.com.png'

const Background = () => {
  return (
    <div className={background.Container}>
      <img className={background.Stars} src={stars} alt='stars' />
    </div>
  )
}

export default Background
