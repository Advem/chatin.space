import io from 'socket.io-client'
import { reset } from 'redux-form'
import { ENDPOINT } from '../ENDPOINT'
import {
  SIGN_IN,
  INSPECT_ROOM,
  DEINSPECT_ROOM,
  SELECT_ROOM,
  OPEN_MENU,
  CLOSE_MENU,
  OPEN_SETTINGS,
  CLOSE_SETTINGS,
  CREATE_ROOM,
  FETCH_ROOMS,
  OPEN_ROOM_FORM,
  CLOSE_ROOM_FORM,
  SET_APP_COLOR,
  SET_USER_COLOR,
  CREATE_SOCKET,
  CHECK_USERNAME,
  UPDATE_ROOM_USERS,
  UPDATE_ROOMS,
  ADD_SERVER_MESSAGE,
  DELETE_SERVER_MESSAGE,
  FETCH_USERS,
  UPDATE_MESSAGES,
  ADD_USER,
  REMOVE_USER,
  REMOVE_MESSAGE,
  ENABLE_USERS_REORDER
} from './types'

export const signIn = userName => {
  return {
    type: SIGN_IN,
    payload: userName
  }
}

export const inspectRoom = id => {
  return {
    type: INSPECT_ROOM,
    payload: id
  }
}

export const deInspectRoom = () => {
  return {
    type: DEINSPECT_ROOM
  }
}

export const selectRoom = (id, name) => {
  return {
    type: SELECT_ROOM,
    payload: { id, name }
  }
}

export const openMenu = () => {
  return {
    type: OPEN_MENU
  }
}

export const closeMenu = () => {
  return {
    type: CLOSE_MENU
  }
}

export const openSettings = () => {
  return {
    type: OPEN_SETTINGS
  }
}

export const closeSettings = () => {
  return {
    type: CLOSE_SETTINGS
  }
}

export const openRoomForm = () => {
  return {
    type: OPEN_ROOM_FORM
  }
}

export const closeRoomForm = () => {
  return {
    type: CLOSE_ROOM_FORM
  }
}

export const setAppColor = appColor => {
  return {
    type: SET_APP_COLOR,
    payload: appColor
  }
}

export const setUserColor = userColor => {
  return {
    type: SET_USER_COLOR,
    payload: userColor
  }
}

export const enableUsersReorder = () => {
  return {
    type: ENABLE_USERS_REORDER
  }
}

export const fetchRooms = () => async (dispatch, getState) => {
  const { socket } = getState().socket
  await socket.emit('fetchRooms', callback => {
    dispatch({ type: FETCH_ROOMS, payload: callback })
  })
}

export const createRoom = formValues => async (dispatch, getState) => {
  const { socket } = getState().socket
  const { userName } = getState().auth
  const data = { ...formValues, userName }
  await socket.emit('createRoom', data, callback => {
    dispatch({ type: CREATE_ROOM, payload: callback })
    dispatch({
      type: SELECT_ROOM,
      payload: { id: callback.id, name: callback.name }
    })
  })
}

export const updateRooms = data => {
  return {
    type: UPDATE_ROOMS,
    payload: data
  }
}
// Server
export const createSocket = () => {
  let socket
  socket = io(ENDPOINT) // connect to Server
  return {
    type: CREATE_SOCKET,
    payload: socket
  }
}

export const checkUserName = name => async (dispatch, getState) => {
  const { socket } = getState().socket
  await socket.emit('sign', { name }, isUser => {
    dispatch({
      type: CHECK_USERNAME,
      payload: isUser
    })
  })
}

export const updateRoomUsers = (roomId, users) => {
  return {
    type: UPDATE_ROOM_USERS,
    payload: { id: roomId, users: users }
  }
}

export const addServerMessage = message => {
  return {
    type: ADD_SERVER_MESSAGE,
    payload: message
  }
}

export const deleteServerMessage = () => {
  return {
    type: DELETE_SERVER_MESSAGE
  }
}

export const fetchUsers = users => {
  return {
    type: FETCH_USERS,
    payload: users
  }
}

export const addUser = user => {
  return {
    type: ADD_USER,
    payload: { name: user.userName, roomId: user.roomId, lastMessage: 1 }
  }
}

export const removeUser = user => {
  return {
    type: REMOVE_USER,
    payload: user
  }
}

export const sendMessage = message => async (dispatch, getState) => {
  const { socket } = getState().socket
  const { userName } = getState().auth
  const { id } = getState().room.selectedRoom
  socket.emit('sendMessage', { userName, roomId: id, message })
  dispatch(reset('createMessage'))
}

export const updateMessages = data => {
  return {
    type: UPDATE_MESSAGES,
    payload: data
  }
}

export const removeMessage = (userName, timestamp) => {
  return {
    type: REMOVE_MESSAGE,
    payload: { userName, timestamp }
  }
}
