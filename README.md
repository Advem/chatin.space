[![N|Solid](https://gitlab.com/Advem/chatin.space/raw/master/client/public/logo192.png)](https://gitlab.com/Advem/chatin.space)

# chatin.Space

WebChat using React (Engineering Degree Project)

### Presentation

Visit the App: **https://chatin.space**

### Description

Chat web-application which allows users to create own configurable rooms and
send real-time messages to users in the same room. The feature of this app is
that every message sent disappears after specific time period and it is not stored anywhere.
The project is highly focued on UI/UX and frontend side.

### Usage

- Visit the App at _https://chatin.space_
- Sign in by typing your name.
- Use burger icon on top-left corner to display Room List.
- You can switch between any Public Rooms and chat with users inside.
- Check out Room Details by clicking a room from the Room List.
- Send messages by typing text into input field on the bottom of screen.
- Your messages will disappear after specific time period set by Room creator.
- You can create your own Rooms by clicking 'New Galaxy' button at Room List.
- You can access Private Rooms by typing correct password set by Room creator.
- Change App Theme and color of your messages by clicking settings button on the right from message input field.
- Try out Auto-positioning feature which sorts users (next to settings button). The newest messager will display on the bottom of your screen _(scrolling is then disabled)_ **Best on mobile devices!**

### Information

- Author: **Adam Drabik**
- Project: **chatin.Space**
- Title: **WebChat using React** _(Webczat z wykorzystaniem React)_
- Degree: **Engineering Degree**
- University: **Gdańsk University of Technology** _(Politechnika Gdańska)_
- Faculty: **Applied Physics and Mathematics** _(Wydział Fizyki Technicznej i Matematyki Stosowanej)_
- Field: Technical Physics - **Applied Informatics** _(Fizyka Techniczna - Informatyka Stosowana)_
- Promoter: **dr. inż. Bartosz Reichel**

### Technologies:

React, Redux, NodeJS, ExpressJS, Socket.io, AWS, Sass, ECMAScript

### Node dependencies:

- **Client:**
  _create-react-app, react, react-dom, react-scripts, react-emoji, react-scroll-to-bottom, react-redux, redux, redux-form, redux-thunk, lodash, axios, js-sha256, node-sass, glamor, socket.io-client_

- **Server:**
  _express, nodemon, lodash, socket.io_

Copyright (C) 2019 Adam 'Advem' Drabik
