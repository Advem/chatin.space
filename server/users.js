const _ = require('lodash')

const users = {
  ID0000: { name: 'Universe', roomId: 3 },
  ID0001: { name: 'admin', roomId: 3 },
  ID0002: { name: 'admin2', roomId: 3 },
  ID0003: { name: 'admin3', roomId: 3 },
  ID0004: { name: 'admin4', roomId: 3 },
  ID0005: { name: 'admin5', roomId: 3 },
  ID0006: { name: 'admin6', roomId: 3 },
  ID0007: { name: 'admin7', roomId: 3 },
  ID0008: { name: 'admin8', roomId: 3 },
  ID0009: { name: 'admin9', roomId: 3 },
  ID0010: { name: 'admin10', roomId: 3 },
  ID0011: { name: 'admin11', roomId: 3 },
  ID0012: { name: 'admin12', roomId: 3 },
  ID0013: { name: 'admin13', roomId: 3 },
  ID0014: { name: 'admin14', roomId: 3 },
  ID0015: { name: 'admin15', roomId: 3 },
  ID0016: { name: 'admin16', roomId: 3 },
  ID0017: { name: 'admin17', roomId: 3 },
  ID0018: { name: 'admin18', roomId: 3 },
  ID0019: { name: 'admin19', roomId: 3 }
}

const addUser = (id, name, roomId) =>
  _.assign(users, { [id]: { name, roomId } })

// const removeUser = id => _.assign(users, { [id]: { name: null, room: null } })
const removeUser = id => delete users[`${id}`]
// const removeUser = id => _.omit(users, [`${id}`])

const getUser = userName => _.findKey(users, { name: userName })

// const getUserRoom = userId => users[userId]['roomId']
const getUserRoom = userId => users[userId]['roomId']
const getUserName = userId => users[userId]['name']

const getUsersInRoom = roomId => _.filter(users, user => user.roomId === roomId)

// const getUsersInRooms = () => _.mapValues(users, 'roomId')
const getUsersInRooms = () => _.values(_.mapValues(users, 'roomId'))

const getUsersAll = () => {
  console.log(users)
}

module.exports = {
  addUser,
  removeUser,
  getUser,
  getUserRoom,
  getUserName,
  getUsersInRoom,
  getUsersInRooms,
  getUsersAll
}
