const _ = require('lodash')

const rooms = {
  1: {
    id: 1,
    name: 'Milky Way',
    userName: 'Universe',
    privacy: false,
    password: null,
    timeout: 8,
    limit: 100,
    users: 0
  },
  2: {
    id: 2,
    privacy: false,
    password: null,
    name: 'Andromeda',
    timeout: 16,
    limit: 60,
    userName: 'Universe',
    users: 0
  },
  3: {
    id: 3,
    privacy: false,
    password: null,
    name: 'Cigar Galaxy',
    timeout: 25,
    limit: 30,
    userName: 'Universe',
    users: 20
  },
  4: {
    id: 4,
    privacy: false,
    password: null,
    name: 'Pinwheel Galaxy',
    timeout: 30,
    limit: 30,
    userName: 'Universe',
    users: 0
  },
  5: {
    id: 5,
    privacy: false,
    password: null,
    name: 'Whrilpool Galaxy',
    timeout: 10,
    limit: 30,
    userName: 'Universe',
    users: 0
  },
  6: {
    id: 6,
    privacy: false,
    password: null,
    name: 'The Void',
    timeout: 1,
    limit: 20,
    userName: 'Universe',
    users: 0
  },
  7: {
    id: 7,
    privacy: false,
    password: null,
    name: "Hoags' Object",
    timeout: 5,
    limit: 20,
    userName: 'Universe',
    users: 0
  },
  8: {
    id: 8,
    privacy: false,
    password: null,
    name: 'Sombrero Galaxy',
    timeout: 60,
    limit: 30,
    userName: 'Universe',
    users: 0
  },
  9: {
    id: 9,
    privacy: true,
    password:
      '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
    name: 'Admin Galaxy',
    timeout: 300,
    limit: 3,
    userName: 'admin',
    users: 0
  }
}

const fetchRooms = () => rooms

const createRoom = data => {
  var last = _.findLastKey(rooms)
  _.assign(rooms, {
    [++last]: {
      ...data,
      id: last,
      users: 0
    }
  })
  // return rooms[_.keys(rooms).length]
  return rooms[last]
}

// const updateRoomUsers = (roomId, value) =>
//   _.assign(rooms[roomId], { users: rooms[roomId]['users'] + value })

const updateRoomUsers = (roomId, users) =>
  _.assign(rooms[roomId], { users: users })

const fetchNewRoom = () => rooms[_.findLastKey(rooms)]

module.exports = {
  rooms,
  fetchRooms,
  fetchNewRoom,
  createRoom,
  updateRoomUsers
}
