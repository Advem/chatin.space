const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.send('<h1 style="color: #520F85;">chatin.Space Server is running...</h1>')
})

module.exports = router
