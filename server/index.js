const express = require('express')
const socketio = require('socket.io')
const router = require('./router')
const {
  addUser,
  removeUser,
  getUser,
  getUserName,
  getUserRoom,
  getUsersInRoom
} = require('./users')

const {
  fetchRooms,
  createRoom,
  fetchNewRoom,
  updateRoomUsers
} = require('./rooms')

const PORT = process.env.PORT || 5000
const app = express()

// Choose between PRODUCTION (https) or DEVELOPMENT (http) SERVER

/* <DEVELOPMENT SERVER> */
const http = require('http')
const server = http.createServer(app)
/* </DEVELOPMENT SERVER> */

// /* <PRODUCTION SERVER> */
// const https = require('https')
// const fs = require('fs')
// const options = {
//   key: fs.readFileSync('/etc/letsencrypt/live/chatin.space/privkey.pem'),
//   cert: fs.readFileSync('/etc/letsencrypt/live/chatin.space/cert.pem')
// }
// const server = https.createServer(options, app)
// /* </PRODUCTION SERVER> */

const io = socketio(server)

app.use(router)

// SOCKET CONNECT
io.on('connect', socket => {
  console.log(`User connected id: ${socket.id}`)
  addUser(socket.id, null, null) // add empty User

  // TRY TO SIGN IN
  socket.on('sign', ({ name }, callback) => {
    callback(getUser(name))
    // console.log(`SIGN ${name}`)
  })

  // USER JOINS ROOM
  socket.on('join', ({ userName, roomName, roomId }) => {
    console.log(`JOIN ${userName} ${roomName} (${roomId})`)

    // SEND SERVER MESSAGE TO ALL USERS THAT USER JOINED A ROOM
    socket.emit('serverMessage', `You have joined ${roomName}`)
    socket
      .to(`${roomId}`)
      .broadcast.emit('serverMessage', `${userName} joined ${roomName}`)

    // ADD USER TO USERS DATABASE
    addUser(socket.id, userName, roomId)

    // UPDATE ROOMS DATABASE (USERS ONLINE)
    updateRoomUsers(roomId, getUsersInRoom(roomId).length)

    // SEND UPDATED ROOM (USERS ONLINE)
    io.emit('roomChanged', {
      roomId: roomId,
      users: getUsersInRoom(roomId).length
    })

    // JOIN SOCKET WITH ROOM NAMESPACE
    socket.join(`${roomId}`)

    // SEND USERS INFORMATION IN THIS ROOM TO THIS USER
    io.to(`${socket.id}`).emit('fetchUsers', getUsersInRoom(roomId))

    // SEND USER INFORMATION TO USERS IN THE ROOM THIS USER JOINED
    socket.to(`${roomId}`).emit('addUser', { userName, roomId })
  })

  // USER LEAVES ROOM
  socket.on('leave', ({ userName, roomName, roomId }) => {
    console.log(`LEAVE ${userName} ${roomName} (${roomId})`)

    // SEND SERVER MESSAGE TO ALL THAT USER LEFT A ROOM
    socket
      .to(`${roomId}`)
      .broadcast.emit('serverMessage', `${userName} left ${roomName}`)

    // REMOVE USER FROM USERS DATABASE
    removeUser(socket.id)

    // UPDATE ROOMS DATABASE (USERS ONLINE)
    updateRoomUsers(roomId, getUsersInRoom(roomId).length)

    // SEND UPDATED ROOM (USERS ONLINE)
    io.emit('roomChanged', {
      roomId: roomId,
      users: getUsersInRoom(roomId).length
    })

    // UNJOIN SOCKET WITH ROOM NAMESPACE
    socket.leave(`${roomId}`)

    // SEND REMOVE USER TO USERS IN THE ROOM THIS USER LEFT
    socket.to(`${roomId}`).emit('removeUser', userName)
  })

  // SEND ROOMS DATABASE
  socket.on('fetchRooms', callback => {
    callback(fetchRooms())
    // console.log(`FETCHED ROOMS`)
  })

  // USER CREATED A ROOM
  socket.on('createRoom', (data, callback) => {
    // SEND NEW ROOM TO THE USERS WHO CREATED IT
    callback(createRoom(data))

    // SEND USERS IN ROOM UPDATE
    socket.broadcast.emit('updateRooms', fetchNewRoom())
    console.log('ROOM CREATED')
  })

  // RECEIVE MESSAGE
  socket.on('sendMessage', ({ userName, roomId, message }) => {
    console.log(`${userName} to ${roomId} ${message}`)

    // SEND MESSAGE TO USERS IN SAME ROOM AS SENDER
    io.to(`${roomId}`).emit('receiveMessage', { userName, message })
  })

  // SOCKET DISCONNECTED
  socket.on('disconnect', () => {
    // IF SOCKET IS NOT SIGNED IN
    if (getUserName(socket.id)) {
      // console.log()
    } else console.log(`User disconnected id: ${socket.id}`)

    // IF SOCKET IS SIGNED IN (MEANS HE IS IN A ROOM)
    if (getUserRoom(socket.id)) {
      const userName = getUserName(socket.id)
      const roomId = getUserRoom(socket.id)

      // SEND SERVER MESSAGE TO USERS IN SAME ROOM
      socket
        .to(`${roomId}`)
        .broadcast.emit('serverMessage', `${getUserName(socket.id)} left`)
      console.log(`LEFT ${userName} (${roomId})`)

      // REMOVE USER FROM USERS DATABASE
      removeUser(socket.id)

      // UPDATE ROOMS DATABASE (USERES ONLINE)
      updateRoomUsers(roomId, getUsersInRoom(roomId).length)

      // SEND UPDATED ROOM (USERS ONLINCE)
      io.emit('roomChanged', {
        roomId: roomId,
        users: getUsersInRoom(roomId).length
      })

      // UNJOIN SOCKET WITH ROOM NAMESPACE
      socket.leave(`${roomId}`)

      // SEND REMOVE USER TO USERS IN THE ROOM THIS USER LEFT
      socket.to(`${roomId}`).emit('removeUser', userName)
      console.log(`DISCONNECTED ${userName}`)
    }
  })
})

server.listen(PORT, () => console.log(`Server started on port ${PORT}`))
